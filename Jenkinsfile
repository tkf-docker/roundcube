def loadConfigYaml()
{
  def valuesYaml = readYaml (file: './config.yaml')
  return valuesYaml;
}

pipeline {
  agent {
    // By default run stuff on a x86_64 node, when we get
    // to the parts where we need to build an image on a diff
    // architecture, we'll run that bit on a diff agent
    label 'X86_64'
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '5'))
    parallelsAlwaysFailFast()
  }

  // Configuration for the variables used for this specific repo
  environment {
    CONTAINER_NAME = 'roundcube'
    LOCAL_DOCKER_REGISTRY = 'artifactory.cosprings.teknofile.net/tkf-docker'
  }

  stages {
    // Setup all the basic enviornment variables needed for the build
    stage("Setup ENV variables") {
      steps {
        script {
          env.EXIT_STATUS = ''
          env.CURR_DATE = sh(
            script: '''date '+%Y-%m-%d_%H:%M:%S%:z' ''',
            returnStdout: true).trim()
          env.GITHASH_SHORT = sh(
            script: '''git log -1 --format=%h''',
            returnStdout: true).trim()
          env.GITHASH_LONG = sh(
            script: '''git log -1 --format=%H''',
            returnStdout: true).trim()
          env.GIT_TAG_NAME = sh(
            script: '''git describe --tags ${commit}''',
            returnStdout: true).trim()
          currentBuild.displayName = "${GIT_TAG_NAME}"

          configYaml = loadConfigYaml()

          env.BASE_TAG = configYaml.baseimage.version
          env.ROUNDCUBE_VERSION = configYaml.roundcube.version
        }
      }
    }
    stage('Build Containers') {
      agent {
        label 'X86_64'
      }
      steps {
        script {
          withDockerRegistry(credentialsId: 'teknofile-dockerhub') {
            sh '''
              docker buildx create --bootstrap --use --name tkf-builder
              docker buildx build \
                --no-cache \
                --pull \
                --build-arg ROUNDCUBE_VERSION=${ROUNDCUBE_VERSION} \
                --build-arg BASE_IMG=${BASE_TAG} \
                --platform linux/amd64 \
                --platform linux/arm64 \
                -t teknofile/${CONTAINER_NAME}:${ROUNDCUBE_VERSION} \
                -t teknofile/${CONTAINER_NAME}:latest \
                -t teknofile/${CONTAINER_NAME} \
                . \
                --push
            '''
          }
          withDockerRegistry(url: 'https://artifactory.cosprings.teknofile.net/tkf-docker', credentialsId: 'tkf-jenkins-artifactory') {
            sh '''
              docker pull teknofile/${CONTAINER_NAME}:${ROUNDCUBE_VERSION}
              docker tag teknofile/${CONTAINER_NAME}:${ROUNDCUBE_VERSION} ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME}:${ROUNDCUBE_VERSION}
              docker tag teknofile/${CONTAINER_NAME}:${ROUNDCUBE_VERSION} ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME}:latest
              docker tag teknofile/${CONTAINER_NAME}:${ROUNDCUBE_VERSION} ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME}
              docker push ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME} -a
            '''
          }
        }
      }
    }
  }
  post {
    cleanup {
      cleanWs()
	    deleteDir()
    }
  }
}
